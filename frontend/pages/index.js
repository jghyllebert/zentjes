import React, { useState } from 'react';
import Head from 'next/head';
import SearchBar from '../components/searchBar';
import SearchResults from '../components/searchResults';

const Home = () => {
  const [searchTerm, setSearchTerm] = useState('')

  const updateSearchTerm = (event) => setSearchTerm(event.target.value);

  return (
    <div>
      <Head>
        <title>Home</title>
        <link rel='icon' href='/static/favicon.ico' importance='low' />
      </Head>

      <div className='hero'>
        <SearchBar change={updateSearchTerm} searchTerm={searchTerm} />
      </div>
      <SearchResults searchTerm={searchTerm} />
    </div>
  )
}


export default Home
