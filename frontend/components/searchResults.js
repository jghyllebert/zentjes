import React from 'react';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';

const SEARCH_PERSONS = gql`
query Search($searchTerm: String!){
  personsSearch(query: $searchTerm) {
    id, firstName, lastName, placeOfBirth, dateOfBirth,
    placeOfDeath, dateOfDeath
  }
}
`;

const SearchResults = ({ searchTerm }) => {
  const { loading, error, data } = useQuery(SEARCH_PERSONS, {
    variables: { searchTerm }
  });
  let results = null;
  if (data) {
    results = (
      <section>
        {data.personsSearch.map(person => (
          <article key={person.id}>
            <header>
              {person.firstName} {person.lastName}
            </header>
          </article>
        ))}
      </section>
    );
  }
  return results;
}

export default SearchResults;
