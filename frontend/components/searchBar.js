import React from 'react';


const SearchBar = ({change, searchTerm}) => (
  <div id="search">
    <input 
      type="text" 
      name="search" 
      id="search--bar"
      onChange={change}
      value={searchTerm}
    />
  </div>
);

export default SearchBar;
