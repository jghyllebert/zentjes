from pytest import mark, raises

from core.utils.exceptions import InputError
from core.utils.clean_input import clean_date, clean_name, get_war_category


@mark.parametrize("name,first_name,last_name", [
    ("GHYLLEBERT  Jonas", 'Jonas', 'Ghyllebert'),
    ("ADAM  Aloïs", 'Aloïs', 'Adam'),
])
def test_clean_name_simple(name, first_name, last_name):
    data = clean_name(name)
    assert data['first_name'] == first_name
    assert data['last_name'] == last_name
    assert data['extra_data'] is None


@mark.parametrize("name,first_name,last_name", [
    ('ALLEMEERSCH  Michel Gabriel', 'Michel Gabriel', 'Allemeersch'),
    ('ACKAERT  Adriaan-Joseph', 'Adriaan-Joseph', 'Ackaert'),
    (
        'de GHELDERE  Jan-Baptist, Marguerite, Camille, Ghislain',
        'Jan-Baptist, Marguerite, Camille, Ghislain',
        'De Gheldere'
    ),
])
def test_clean_name_multiple_names(name, first_name, last_name):
    data = clean_name(name)
    assert data['last_name'] == last_name
    assert data['first_name'] == first_name


@mark.parametrize("input", ['', None])
def test_clean_name_no_input(input):
    with raises(InputError):
        clean_name(input)


@mark.parametrize("input,expected", [
    ("(foto)", 'foto)'),
    (" (dubbel zentje met man)", 'dubbel zentje met man)'),
    ("(foto)(dubbel zentje met man)", 'foto)(dubbel zentje met man)')
])
def test_clean_name_extra_data(input, expected):
    data = clean_name(f'GHYLLEBERT  Jonas          {input}')
    assert data['first_name'] == 'Jonas'
    assert data['extra_data'] == expected


def test_clean_date_only_year():
    cleaned_date = clean_date('00/00/1776')
    assert cleaned_date.date.year == 1776

def test_clean_date_no_days():
    cleaned_date = clean_date('00/09/1807')
    assert cleaned_date.date.year == 1807
    assert cleaned_date.date.month == 9

def test_clean_date_no_input():
    cleaned_date = clean_date('')
    assert cleaned_date is None


@mark.parametrize("input,death_year,expected", [
    ('O.S. 1914-1918', 1958, 'V_WW1'),
    ('O.S. 1940-1945', 1988, 'V_WW2'),
    ('O.S. 1940', 1950, 'V_WW2'),
    ('(gesneuvelde soldaat W.O. 1914-1918)', 1917, 'D_WW1'),
    ('(gesneuvelde soldaat) (O.S. 1940-1945)', 1940, 'D_WW2'),
    ('oorlogsslachtoffer', 1940, 'D_WW2'),
])
def test_get_war_category(input, death_year, expected):
    category = get_war_category(input, death_year)
    assert category == expected
