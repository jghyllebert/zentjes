from elasticsearch_dsl.query import Q
import graphene
from graphene_django import DjangoObjectType
from graphene_file_upload.scalars import Upload

from .documents import PersonDocument
from .models import InMemoriamCard, Person


class PersonType(DjangoObjectType):
    class Meta:
        model = Person

    def resolve_date_of_birth(self, info):
        return self.date_of_birth.format('%Y', '%m/%Y', '%d/%m/%Y')
    
    def resolve_date_of_death(self, info):
        return self.date_of_death.format('%Y', '%m/%Y', '%d/%m/%Y')


class InMemoriamCardType(DjangoObjectType):
    class Meta:
        model = InMemoriamCard

    def resolve_cover(self, info):
        return self.cover.url


class PersonMutation(graphene.Mutation):
    class Arguments:
        first_name = graphene.String(required=True)
        last_name = graphene.String(required=True)
        date_of_birth = graphene.String(required=False)
        place_of_birth = graphene.String(required=False)
        date_of_death = graphene.String(required=False)
        place_of_death = graphene.String(required=False)
    
    person = graphene.Field(PersonType)
    
    def mutate(self, info, **kwargs):
        person = Person.objects.create(
            first_name=kwargs['first_name'],
            last_name=kwargs['last_name'],
            date_of_birth=kwargs.get('date_of_birth'),
            place_of_birth=kwargs.get('place_of_birth'),
            date_of_death=kwargs.get('date_of_death'),
            place_of_death=kwargs.get('place_of_death'),
        )
        person.save()
        return PersonMutation(person=person)


class InMemoriamCardMutation(graphene.Mutation):
    class Arguments:
        card = graphene.String(required=False)
        person = graphene.String(required=False)
        cover = Upload(required=False)
        back = Upload(required=False)
    
    card = graphene.Field(InMemoriamCardType)

    def mutate(self, info, **kwargs):
        # TODO validate for either person or card
        cover = kwargs.get('cover')
        back = kwargs.get('back')
        person = Person.objects.get(id=kwargs['person'])
        if kwargs.get('card'):
            card = InMemoriamCard.objects.get(id=kwargs['card'])
        else:
            card = InMemoriamCard(person=person)
        if cover:
            card.cover = cover
        if back:
            card.back = back
        card.save()
        return InMemoriamCardMutation(card=card)


class Mutation(graphene.ObjectType):
    add_person = PersonMutation.Field()
    add_in_memoriam_card = InMemoriamCardMutation.Field()


class Query:
    persons_search = graphene.List(PersonType, query=graphene.String())
    person = graphene.Field(PersonType, person_id=graphene.String())

    def resolve_persons_search(self, info, query):
        dsl_persons = PersonDocument.search()
        dsl_persons = dsl_persons.query(
            "multi_match",
            query=query,
            fuzziness=10,
            fields=[
                'first_name', 'last_name', 'place_of_death', 'place_of_birth'
            ])
        return dsl_persons.to_queryset()

    def resolve_person(self, info, person_id):
        return Person.objects.get(id=person_id)
