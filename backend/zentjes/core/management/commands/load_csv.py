import csv

from django.core.management import BaseCommand
from tqdm import tqdm

from core.models import Person
from core.utils.constants import WW1_END, WW1_START, WW2_END, WW2_START
from core.utils.clean_input import clean_date, clean_name, get_war_category
from core.utils.exceptions import InputError


class Command(BaseCommand):
    help = 'Load data from csv'

    def add_arguments(self, parser):
        parser.add_argument('file', type=str)

    def handle(self, *args, **options):
        added = 0
        duplicates = 0
        errors = 0
        with open(options['file']) as f:
            reader = csv.reader(f, delimiter=",")
            next(reader, None)  # Skip headers in CSV file
            for row in tqdm(reader):
                try:
                    data = clean_name(row[0])
                    extra_data = data['extra_data']
                    del data['extra_data']
                except InputError:
                    self.stdout.write(
                        self.style.ERROR(f'error with: {row[0]}'))
                    errors += 1
                    continue
                try:
                    data['date_of_birth'] = clean_date(row[2])
                    data['date_of_death'] = clean_date(row[4])
                    if row[1]:
                        data['place_of_birth'] = row[1].split('(')[0].strip()
                    if row[3]:
                        data['place_of_death'] = row[3].split('(')[0].strip()
                except Exception:
                    errors += 1
                    continue

                try:
                    person = Person.objects.get(**data)
                    duplicates += 1
                except Person.DoesNotExist:
                    person = Person.objects.create(**data)
                    person.save()
                    added += 1
                except Person.MultipleObjectsReturned:
                    self.stdout.write(self.style.ERROR(
                        'Got multiple {first_name} {last_name}'.format(**data)
                    ))
                
                if not person.category:
                    try:
                        death_year = data['date_of_death'].date.year
                        person.category = get_war_category(
                            extra_data, death_year)
                        person.save()
                    except Exception:
                        errors += 1
                        continue
        self.stdout.write(self.style.SUCCESS(f'Added {added} new persons'))
        if duplicates:
            self.stdout.write(
                self.style.NOTICE(f'Encountered {duplicates} duplicates'))
        if errors:
            self.stdout.write(self.style.ERROR(f'{errors} errors'))
        self.stdout.write(self.style.SUCCESS('DONE'))
