from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry

from .models import Person


@registry.register_document
class PersonDocument(Document):
    class Index:
        name = 'persons'
        settings = {
            'number_of_shards': 1,
            'number_of_replicas': 0
        }
    
    class Django:
        model = Person
        fields = [
            'first_name',
            'last_name',
            'place_of_birth',
            'place_of_death'
        ]