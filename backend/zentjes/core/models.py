import uuid

from django.db import models
from partial_date import PartialDateField


SPECIAL_CATEGORY = (
    ('V_WW1', 'VETERAN WW1'),
    ('D_WW1', 'WW1 KIA'),
    ('V_WW2', 'VETERAN WW2'),
    ('D_WW2', 'WW2 KIA'),
)


class InMemoriamCard(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    cover = models.ImageField(upload_to='images/', null=True, blank=True)
    back = models.ImageField(upload_to='images/', null=True, blank=True)
    person = models.ForeignKey(
        "core.Person", null=True, blank=True, on_delete=models.PROTECT)


class Person(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    first_name = models.CharField(max_length=175, null=False, blank=False)
    last_name = models.CharField(max_length=175, null=False, blank=False)
    place_of_birth = models.CharField(max_length=75, null=True, blank=True)
    place_of_death = models.CharField(max_length=75, null=True, blank=True)
    date_of_birth = PartialDateField(null=True, blank=True)
    date_of_death = PartialDateField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    spouses = models.ManyToManyField("self")
    category = models.CharField(
        max_length=5,
        choices=SPECIAL_CATEGORY,
        default=None,
        null=True,
        blank=True
    )
    other_category_name = models.TextField(blank=True, null=True)

    class Meta:
        indexes = [
            models.Index(fields=['first_name', 'last_name']),
            models.Index(fields=['place_of_death']),
            models.Index(fields=['place_of_birth'])
        ]