from partial_date import PartialDate

from .constants import (
    NAMES_DELIMITER,
    WAR_DEATHS, 
    WW1_END,
    WW1_START,
    WW1_YEARS, 
    WW2_END,
    WW2_START,
    WW2_YEARS,
)
from .exceptions import InputError


def clean_name(raw_name):
    """Cleans the name input"""
    if not raw_name:
        raise InputError

    try:
        last_name, extra = raw_name.split(NAMES_DELIMITER, maxsplit=1)
    except ValueError:
        raise InputError
    cleaned_extra = ' '.join(extra.split())
    data = extra.split('(', maxsplit=1)
    first_name = data[0].strip()
    extra_data = data[1].strip() if len(data) > 1 else None

    return {
        'first_name': first_name.title(),
        'last_name': last_name.title(),
        'extra_data': extra_data,
    }


def clean_date(raw_date):
    """Clean the birth and death dates.
    
    Some entries have partial dates which need to be accounted for.

    Args:
        raw_date (str): retrieved date from data set
    Returns:
        PartialDate object
    """
    if not raw_date:
        return
    day, month, year = raw_date.split('/', maxsplit=2)
    build_date = year
    if month and month != '00':
        build_date += f'-{month}'
    if day and day != '00':
        build_date += f'-{day}'
    return PartialDate(build_date)


def get_war_category(data, year_of_death):
    category = None
    rest_split = set(data.split('('))
    if len(rest_split) >= 1: 
        categories = [other.split(')')[0] for other in rest_split]
        for found_category in categories:
            if 'O.S.' in found_category.upper():
                if any(year in found_category for year in WW1_YEARS):
                    category = 'V_WW1' if category is None else category
                elif any(year in found_category for year in WW2_YEARS):
                    category = 'V_WW2' if category is None else category
            if any(death in found_category.lower() for death in WAR_DEATHS):
                if(WW1_START <= year_of_death <= WW1_END):
                    category = 'D_WW1'
                elif(WW2_START <= year_of_death <= WW2_END):
                    category = 'D_WW2'
    return category
