WW1_START = 1914
WW1_END = 1918
WW2_START = 1939
WW2_END = 1945
WW1_YEARS = ['14', '15', '16', '17', '18']
WW2_YEARS = ['39', '40', '41', '42', '43', '44', '45']
WAR_DEATHS = ['oorlogsslachtoffer', 'gesneuv']

NAMES_DELIMITER = '  '
"""Dataset has two spaces between last names and first_names"""