from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import graphene
from graphene_django.converter import convert_django_field
from graphene_file_upload.django import FileUploadGraphQLView
from partial_date import PartialDateField


@convert_django_field.register(PartialDateField)
def convert_partial_field_to_string(field, registry=None):
    return graphene.String()

from core.graphql import Mutation as CoreMutation, Query as CoreQuery


class Query(CoreQuery, graphene.ObjectType):
    pass


class Mutation(CoreMutation, graphene.ObjectType):
    pass


class GraphQLView(FileUploadGraphQLView):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, args, kwargs)


schema = graphene.Schema(query=Query, mutation=Mutation)
